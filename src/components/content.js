import "./nav.css";
import React from "react";
import { Link } from "react-router-dom";
import {
  FaRegNewspaper,
  FaListUl,
  FaRegBuilding,
  FaBell,
  FaProductHunt,
  FaBars
} from "react-icons/fa";
function Navigation() {
  return (
    <>
      <div className="container-fluid" style={{ backgroundColor: "black" }}>
        <nav className="navbar navbar-expand-lg navbar-light">
          <div class="container-md">
            <a className="navbar-brand brand bg-white" href="/">
              <span className="logo text-center"> Z</span>
            </a>
            <button
              className="navbar-toggler"
              style={{ backgroundColor: "black" }}
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#navbar"
              aria-controls="navbar"
              aria-label="Toggle navigation"
            >
              <FaBars style={{ color: "white" }}></FaBars>
            </button>

            <div className="collapse navbar-collapse" id="navbar">
              <form className="my-4 my-lg-0 frm">
                <input
                  className="form-control"
                  type="search"
                  placeholder="Search"
                />
              </form>

              <ul className="navbar-nav ms-auto">
                <li className="nav-item ms-4">
                  <center>
                    <FaRegNewspaper class="Fa-RegNewspaper text-center img"></FaRegNewspaper>
                  </center>
                  <Link
                    to="/signup"
                    className="nav-link text-center nav-links   text-white"
                  >
                    Feed
                  </Link>
                </li>
                <li className="nav-item ms-4">
                  <center>
                    <FaListUl class="Fa-RegNewspaper text-center  img"></FaListUl>
                  </center>
                  <Link
                    to="/jobs"
                    className="nav-link  nav-links text-center  text-white"
                    active
                  >
                    Jobs
                  </Link>
                </li>
                <li className="nav-item ms-4">
                  <center>
                    <FaBell class="Fa-RegNewspaper text-center   img"></FaBell>
                  </center>

                  <Link
                    to="/notification"
                    className="nav-link text-center nav-links  text-white"
                  >
                    Notification
                  </Link>
                </li>
                <li className="nav-item ms-4">
                  <center>
                    <FaRegBuilding class="Fa-RegNewspaper text-center   img"></FaRegBuilding>
                  </center>
                  <Link
                    to="/bazaar"
                    className="nav-link text-center nav-links text-center  text-white"
                  >
                    Bazaar
                  </Link>
                </li>
                <li className="nav-item ms-4">
                  <center>
                    <FaProductHunt class="Fa-RegNewspaper text-center img"></FaProductHunt>
                  </center>
                  <Link
                    to="/myprofile"
                    className="nav-link text-center nav-links text-center text-white"
                  >
                    Myprofile
                  </Link>
                </li>
              </ul>
            </div>
          </div>
        </nav>
      </div>
    </>
  );
}

export default Navigation;
