import React, { useState } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Apps from "./app";
import { AlertContext } from "./context/alertcontext";
import Signin from "./signin";
import Signup from "./signup";
export default function App() {
  const [alert, setAlert] = useState("");
  return (
    <Router>
      <Switch>
        <AlertContext.Provider value={{ alert, setAlert }}>
          <Route path="/" exact component={Signin} />
          <Route path="/signup" component={Apps} />
          <Route path="/login" component={Signup} />
          <Route path="/register" component={Signin} />
        </AlertContext.Provider>
      </Switch>
    </Router>
  );
}
