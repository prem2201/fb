import { useFormik } from "formik";
import React, { useContext, useState } from "react";
import { useHistory } from "react-router-dom";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import * as yup from "yup";
import "./components/nav.css";
import { AlertContext } from "./context/alertcontext";
function Signin() {
  const { alert, setAlert } = useContext(AlertContext);
  const [hideden, setHidden] = useState("");
  let history = useHistory();
  const user =
    /^(?:[A-Z\d][A-Z\d_-]{5,10}|[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4})$/i;
  const formik = useFormik({
    initialValues: {
      name: "",
      password: "",
    },
    validationSchema: yup.object({
      name: yup
        .string()
        .required("this field is required")
        .matches(user, "not valid"),
      password: yup
        .string()
        .required("passsword is required")
        .min(8, "minimum 8 characters required"),
    }),
    onSubmit: (data) => {
      var axios = require("axios");
      var config = {
        method: "post",
        url: "https://mocker-collections-api.herokuapp.com/user/login",
        headers: {
          "Content-Type": "application/json",
        },
        data: data,
      };
      axios(config)
        .then(function (response) {
          console.log(JSON.stringify(response.data));
          setAlert("success");
          setHidden("alert alert-success");
          setTimeout(function () {
            history.push("/signup");
            setAlert("");
          }, 1000);
        })
        .catch(function (error) {
          setAlert("error");
          setHidden("alert alert-danger");
          setTimeout(function () {
            setAlert("");
          }, 1000);
          var config = {
            method: "post",
            url: "https://mocker-collections-api.herokuapp.com/user/login",
            headers: {
              "Content-Type": "application/json",
            },
            data: data,
          };
          axios(config)
            .then(function (response) {
              console.log(JSON.stringify(response.data));
            })
            .catch(function (error) {
              toast.error(error);
            });
        });
    },
  });
  return (
    <>
      <div className="container-sm mt-4 p-3">
        <div class="center rounded d-flex row ">
          <div class="col-lg-5 col-sm-8 col-sm-10 mt-3">
            <div class={hideden} role="alert">
              {alert}
            </div>
            <div class="shadow-lg p-4">
              <h1 class="mt-3">Sign-in</h1>
              <form class="form" onSubmit={formik.handleSubmit}>
                <input
                  type="text"
                  name="name"
                  class="w-100 input mt-3"
                  placeholder="Username or Email"
                  style={{
                    padding: "13px",
                    borderRadius: "5px",
                    border: "1px solid grey",
                  }}
                  onChange={formik.handleChange}
                  value={formik.values.name}
                />
                {formik.errors.name ? (
                  <div class="text-danger">{formik.errors.name}</div>
                ) : null}

                <input
                  type="password"
                  name="password"
                  class="w-100 input mt-3"
                  placeholder="Password"
                  style={{
                    padding: "13px",
                    borderRadius: "5px",
                    border: "1px solid grey",
                  }}
                  onChange={formik.handleChange}
                  value={formik.values.password}
                />
                {formik.errors.password ? (
                  <div class="text-danger">{formik.errors.password}</div>
                ) : null}

                <button
                  type="submit"
                  class="w-100 text-white  btn  mt-3"
                  style={{ padding: "13px", backgroundColor: "#382f9c" }}
                >
                  <b>Signin</b>
                </button>
                <br />
                <button
                  class="w-100 text-white  btn btn-primary mt-3"
                  style={{ padding: "13px" }}
                  onClick={() => {
                    history.push("/login");
                  }}
                >
                  Create an account
                </button>

                <br />
              </form>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default Signin;
