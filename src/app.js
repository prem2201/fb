import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Navigation from "./components/content";
import Feed from "./pages/feed";
import Jobs from "./pages/jobs";
import Note from "./pages/notification";
import Bazzar from "./pages/bazaar";
export default function App() {
  return (
    <Router>
      <Navigation />
      <Switch>
        <Route path="/signup" exact component={Feed} />
        <Route path="/jobs" component={Jobs} />
        <Route path="/bazaar" component={Bazzar} />
        <Route path="/notification" component={Note} />
      </Switch>
    </Router>
  );
}
