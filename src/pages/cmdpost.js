import "bootstrap/dist/css/bootstrap.min.css";
import React, { useState } from "react";
import Dum from "./dummy";
import DataJson from "./post.json";
const Cmdpost = () => {
  const [color, setcolor] = useState("#ADADAD");
  const [count, setcount] = useState(0);
  const [data, setData] = useState(DataJson);

  // const [hidden, sethidden] = useState(false);
  const render = data?.post?.map((post) => (
    <>
      <div className="p-3 mb-5 mt-5 w-100 ">
        <div className="row center box-2">
          <div className="col-lg-12 col-md-12 col-sm-4">
            <center>
              <img className="img-fluid" src={post.src} alt="" />
            </center>
          </div>
        </div>
        <Dum />
      </div>
    </>
  ));
  return <div>{render}</div>;
};
export default Cmdpost;
