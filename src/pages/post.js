import "bootstrap/dist/css/bootstrap.min.css";
import React, { useState } from "react";
import { Collapse } from "react-bootstrap";
import { FaComment, FaHeart, FaShare } from "react-icons/fa";
import Cmd from "./dummy";
import DataJson from "./post.json";
const NewsFeed = () => {
  const [color, setcolor] = useState("#ADADAD");
  const [count, setcount] = useState(0);
  const [data, setData] = useState(DataJson);
  const [open, setOpen] = useState(false);

  // const [hidden, sethidden] = useState(false);
  const like = () => {
    setcolor("red");
    setcount(count + 1);
  };

  const render = data?.post?.map((post) => (
    <>
      <div className="p-3 mb-5 mt-5 w-100 ">
        <div className="d-flex row  center">
          <div className="d-flex  col-md-8 col-sm-12 col-lg-5 box3 p-3">
            <img
              className="rounded-circle ml-3"
              width="70"
              height="70"
              src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcThy4PDvvASgiDmErdRQ789so1tKKTeZ3nNOw&usqp=CAU"
            />
            <div className="col-md-4 col-sm-7 ms-4">
              <h5 className="font-wight-bold mt-3">M.S.Dhoni </h5>
              <p>indian cricketer</p>
            </div>

            <div className="col-md-2 col-sm-5 col-lg-5 mt-md-3 ">
              <p style={{ color: "#ADADAD" }}>Just Now</p>
            </div>
          </div>
        </div>
        <div className="row center box-2">
          <div className="col-lg-5 col-md-8  box4">
            <div className="p-3">{post.content}</div>
            <img className="img-fluid w-100" src={post.src} alt="" />
            <div className="p-3">
              <FaHeart
                style={{ color }}
                onClick={like}
                className="fa-lg ms-4  cursor"
              />
              <span class="p-2">{count} Likes</span>
              <a
                style={{ backgroundColor: "transparent", border: "none" }}
                class="btn p-3"
                onClick={() => setOpen(!open)}
                aria-controls="example-collapse-text"
                aria-expanded={open}
              >
                <FaComment style={{ color: "#ADADAD" }} />
                <span class="p-2">Comment</span>
              </a>
              <Collapse in={open}>
                <div id="example-collapse-text">
                  <center>
                    <img
                      className="img-fluid w-75 mt-4"
                      src={post.src}
                      alt=""
                    />
                  </center>
                  <Cmd />
                </div>
              </Collapse>
              <span class="ms-3">
                <FaShare style={{ color: "#ADADAD" }} />
                <span class="p-2">Share</span>
              </span>
            </div>
          </div>
        </div>
      </div>
    </>
  ));

  return <div>{render}</div>;
};
export default NewsFeed;
