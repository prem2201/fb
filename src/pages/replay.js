import React, { useState } from "react";
import { Button } from "react-bootstrap";
import { Collapse } from "react-bootstrap";
import ReactDOM from "react-dom";
import { Accordion } from "bootstrap";
import { Card } from "bootstrap";
import { Toggle } from "bootstrap";
import Cmd from "./cmdpost";
import { MdSend } from "react-icons/md";
import { BsReplyFill } from "react-icons/bs";
const AddReplay = ({ addReplay }) => {
  const [value, updateValue] = useState("");
  const handleSubmit = (e) => {
    e.preventDefault();
    if (value !== "") {
      addReplay(value);
      updateValue("");
    }
  };

  return (
    <div>
      <div class="row">
        <div class="col-2"></div>
        <div class="col-10">
          <form onSubmit={handleSubmit}>
            <div class="input-group mb-3d-flex">
              <input
                id="cmd"
                type="text"
                value={value}
                class="form-control"
                placeholder="Enter Your Replay"
                style={{
                  border: "none",
                  padding: "10px",
                  borderRadius: "20px",
                  backgroundColor: "rgb(238, 235, 235)"
                }}
                onChange={(e) => updateValue(e.target.value)}
              />
              <div class="input-group-append ">
                <span class="input-group-text btn">
                  <button
                    type="submit"
                    style={{ borderRadius: "20px", display: "none" }}
                    class="btn btn-sm btn-success ms-2"
                  >
                    &nbsp;
                    <MdSend />
                  </button>
                </span>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};
const Replay = () => {
  const addReplay = (text) => updatetask([...tasks, { text }]);
  const [tasks, updatetask] = useState([""]);
  const [open, setOpen] = useState(false);
  return (
    <div>
      {tasks.map((task) => (
        <div className="replay-status">
          <BsReplyFill class="text-danger" /> {task.text}
        </div>
      ))}
      <AddReplay addReplay={addReplay} />
    </div>
  );
};
export default Replay;
