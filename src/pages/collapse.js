import React, { useState } from "react";
import { Button } from "react-bootstrap";
import { Collapse } from "react-bootstrap";
import Replay from "./replay";
const Collapsebar = () => {
  const [open, setOpen] = useState(false);

  return (
    <>
      <a
        style={{
          textDecoration: "underline",
          cursor: "pointer",
          fontSize: "13px"
        }}
        onClick={() => setOpen(!open)}
        aria-controls="example-collapse-text"
        aria-expanded={open}
      >
        Replay
      </a>
      <Collapse style={{ width: "100%" }} in={open}>
        <div id="example-collapse-text">
          <br />
          <Replay />
        </div>
      </Collapse>
    </>
  );
};
export default Collapsebar;
