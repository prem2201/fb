import React, { useState } from "react";
import { Button } from "react-bootstrap";
import { Collapse } from "react-bootstrap";
import ReactDOM from "react-dom";
import { Accordion } from "bootstrap";
import { Card } from "bootstrap";
import { Toggle } from "bootstrap";
import "./cmd.css";
import Collapsebar from "./collapse";
import { MdSend } from "react-icons/md";
const AddCmd = ({ addCmd }) => {
  const [value, updateValue] = useState("");
  const handleSubmit = (e) => {
    e.preventDefault();
    if (value !== "") {
      addCmd(value);
      updateValue("");
    }
  };

  return (
    <div>
      <div class="row">
        <div class="col-12">
          <form onSubmit={handleSubmit} class="ms-lg-3">
            <div class="input-group mb-3 d-flex">
              <input
                id="cmd"
                type="text"
                value={value}
                class="form-control ms-2"
                placeholder="Enter Your command"
                style={{
                  border: "none",
                  padding: "10px",
                  borderRadius: "20px",
                  backgroundColor: "rgb(238, 235, 235)"
                }}
                onChange={(e) => updateValue(e.target.value)}
              />
              <div class="input-group-append">
                <span class="input-group-text btn">
                  <button
                    type="submit"
                    style={{ borderRadius: "20px", display: "none" }}
                    class="btn btn-sm btn-danger"
                  >
                    &nbsp;
                    <MdSend />
                  </button>
                </span>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};
const Cmd = () => {
  const addCmd = (text) => updatetask([...tasks, { text }]);
  const [tasks, updatetask] = useState([""]);
  const [open, setOpen] = useState(false);
  return (
    <div class="row">
      <div class=" col-lg-11 col-md-11 col-sm-11">
        <div className="list-of-cmd" class="mt-2">
          <div>
            {tasks.map((task) => (
              <div className="cmd-status">
                <b class="cmd"> {task.text}</b>
                <div class="cmd1">
                  <Collapsebar />
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
      <AddCmd addCmd={addCmd} />
    </div>
  );
};
export default Cmd;
