import React, { useState } from "react";
import "./in.css";
import { FaImages } from "react-icons/fa";
import { ToastContainer, toast } from "react-toastify";
import NewsFeed from "./post";
import DataJson from "./post.json";
import { Alert } from "bootstrap";
import { Toast } from "react-bootstrap";
const Postmsg = () => {
  const [img, setimg] = useState("");
  const [data, setData] = useState(DataJson);
  const [hidden, sethidden] = useState(true);
  const [msg, setmsg] = useState("");
  const [hidden1, sethidden1] = useState(false);
  const Typemsg = (e) => setmsg(e.target.value);
  const CHARACTER_LIMIT = 120;

  const deleteimg = () => {
    setimg("");
    sethidden(true);
  };
  const selectimg = (e) => {
    if (img) {
    } else {
      setimg(URL.createObjectURL(e.target.files[0]));
      sethidden(false);
    }
  };
  function notify() {
    toast.dark("Hey 👋, see how easy!");
  }
  const Savepost = () => {
    debugger;
    if (msg || img) {
      sethidden1(true);
      var obj = {
        content: msg,
        src: img
      };
      data.post.push(obj);
      setData({ ...data });
      setmsg("");
      setimg("");
      sethidden(true);
    } else {
      alert("Please Add img or text");
    }
  };
  return (
    <>
      <div className="d-flex row mt-5 center" style={{ margin: "auto" }}>
        <div className="d-flex col-md-8 col-sm-11 col-lg-5 box1 p-3">
          <img
            className="rounded-circle ms-3"
            width="70"
            height="70"
            src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcThy4PDvvASgiDmErdRQ789so1tKKTeZ3nNOw&usqp=CAU"
          />
          <div className="col-md-3 col-lg-5 col-sm-6">
            <h5 class="ms-2" className="font-wight-bold">
              M.S.Dhoni{" "}
            </h5>
            <p class="ms-2">indian cricketer</p>
          </div>

          <div className="col-sm-5 col-lg-5 mt-md-3 justify-content-start">
            <label
              className="btn label  "
              style={{
                backgroundColor: "white",
                borderRadius: "25px",
                border: "1px solid rgba(238, 238, 238, 0.733)"
              }}
            >
              <input
                type="file"
                name="img"
                accept="image/*"
                style={{ display: "none" }}
                id="uplodebtn"
                onChange={selectimg}
              />
              <FaImages></FaImages>
              <span className="ms-2">Add</span>
            </label>

            <label
              className="btn label ms-2"
              onClick={Savepost}
              style={{
                backgroundColor: "white",
                borderRadius: "25px",
                border: "1px solid rgba(238, 238, 238, 0.733)"
              }}
            >
              <div style={{ display: "none" }}></div>
              <span className="ms-2">Post</span>
            </label>
          </div>
        </div>
      </div>
      <div className="row center">
        <div className="col-lg-5 col-md-5 ">
          <center>
            <div style={{ width: "300px" }}>
              <div hidden={hidden} className="p-2" style={{ width: "300px" }}>
                <div className="delete rounded" onClick={deleteimg}>
                  <img
                    src="https://image.flaticon.com/icons/png/128/1828/1828843.png"
                    height="20px"
                  />
                </div>
              </div>
              <img
                src={img}
                className="dimg"
                width="300px"
                hidden={hidden}
                height="100%"
              />
            </div>
          </center>
        </div>
      </div>
      <div className="row center">
        <div className="col-lg-5 col-md-8 d-flex box2 ">
          <form className="from w-75 mt-3 ms-3">
            <textarea
              className="form-controls ms-3"
              type="search"
              placeholder="Hey! Try Something Here"
              value={msg}
              maxLength="120"
              onChange={Typemsg}
            ></textarea>
          </form>
          <span className="absolute">{`${msg.length}/${CHARACTER_LIMIT}`}</span>
        </div>
      </div>

      <div
        className="container w-50  justify-content-center mt-3"
        hidden={hidden1}
      >
        <hr />
        <center>
          <div
            style={{ backgroundColor: "#f0f0f0", cursor: "pointer" }}
            className="col-lg-6  p-2 border text-center rounded-pill"
          >
            Sorry! No posts available
          </div>
        </center>
      </div>

      <NewsFeed />
    </>
  );
};
export default Postmsg;
